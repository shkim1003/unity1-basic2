﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GuGuDanWhileController : MonoBehaviour {
	private Text[] displayTextArray;
	private string[] displayColorArray = new string[] {
		"ff0000", "ff8c00", "ffff00", "008000", "0000ff", "4b0082", "800080", "000000"
	};

	// Use this for initialization
	void Start () {
		Screen.SetResolution(640, 480, true);

		displayTextArray = new Text[] {
			GameObject.Find ("WNumber2").GetComponent<Text> (),
			GameObject.Find ("WNumber3").GetComponent<Text> (),
			GameObject.Find ("WNumber4").GetComponent<Text> (),
			GameObject.Find ("WNumber5").GetComponent<Text> (),
			GameObject.Find ("WNumber6").GetComponent<Text> (),
			GameObject.Find ("WNumber7").GetComponent<Text> (),
			GameObject.Find ("WNumber8").GetComponent<Text> (),
			GameObject.Find ("WNumber9").GetComponent<Text> ()
		};
	}

	// Update is called once per frame
	void Update () {
	}

	public void StartGame() {
		for (int i=0; i < displayTextArray.Length; i++) {
			displayTextArray [i].text = "";
		}

		StartCoroutine (GuGuDanGameWhile());
	}

	IEnumerator GuGuDanGameWhile() {
		int i = 2;
		while (i < 10) {
			int j = 1;
			StringBuilder builder = new StringBuilder ();
			while (j < 10) {
				builder.Append("<color=#").Append(displayColorArray[i-2]).Append(">").Append (i + " x " + j + " = " + i * j).Append("</color>").Append("\n");
				displayTextArray[i-2].text = builder.ToString();
				j++;
				yield return new WaitForSeconds(.00005f);
			}
			i++;
			yield return new WaitForSeconds(.00005f);
		}
	}

	public void ExitGame() {
		SceneManager.LoadScene ("Main");
	}
}