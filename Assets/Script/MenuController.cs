﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Screen.SetResolution(640, 480, true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenOXGame() {
		SceneManager.LoadScene ("GameOX");
	}

	public void OpenGuGuDanFor() {
		SceneManager.LoadScene ("GameFor");
	}

	public void OpenGuGuDanWhile() {
		SceneManager.LoadScene ("GameWhile");
	}
}
