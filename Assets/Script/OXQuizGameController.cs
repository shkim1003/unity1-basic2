﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OXQuizGameController : MonoBehaviour {
	private ArrayList quizList = new ArrayList();

	public Text displayQuiz;
	public Text displayCount;

	private int answerCount;
	private bool currentAnswer;


	// Use this for initialization
	void Start () {
		Screen.SetResolution(640, 480, true);

		this.initializeQuizList ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DoStartQuiz() {
		answerCount = 0;
		this.displayQuiz.text = "";
		this.displayCount.text = "";
		this.DoGame ();
	}

	public void AnswerO() {
		if (currentAnswer == true) {
			answerCount++;
		}
		displayCount.text = "정답 : " + answerCount;
		this.DoGame ();
	}

	public void AnswerX() {
		if (currentAnswer == false) {
			answerCount++;
		}
		displayCount.text = "정답 : " + answerCount;
		this.DoGame ();
	}

	public void ExitGame() {
		SceneManager.LoadScene ("Main");
	}

	private void initializeQuizList() {
		quizList.Add (new Quiz ("세계 3대 악마게임은 문명, WoW, FM 이다?", false));
		quizList.Add (new Quiz ("페르시아 군은 이집트와 전쟁에서 고양이를 무기로 사용 한적 있다?", true));
		quizList.Add (new Quiz ("갑신정변의 결과로 청일양국 동시 철병을 내용으로 하는 제물포 조약을 맺었다.", false));
		quizList.Add (new Quiz ("카카오프랜지에 라이언은 갈기 없는 사자 이다.", true));
		quizList.Add (new Quiz ("스팸의 굳은기름은 감자 전분 이다?", true));
	}

	private void DoGame() {
		if (quizList.Count == 0) {
			this.EndGame ();
			return;
		}

		Quiz quiz = (Quiz)quizList [Random.Range(0, quizList.Count)];
		quizList.Remove (quiz);

		displayQuiz.text = quiz.quizText;
		currentAnswer = quiz.answer;
	}

	private void EndGame() {
		displayQuiz.text = "수고하셨습니다.";
	}

	public class Quiz {
		public string quizText;
		public bool answer;

		public Quiz(string quiz, bool answer) {
			this.quizText = quiz;
			this.answer = answer;
		}
	}
}
